'use strict';
module.exports = (sequelize, DataTypes) => {
  var Trade = sequelize.define('Trade', {
    id: {
      type: DataTypes.STRING,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      autoIncrement: false
    },
    type: DataTypes.STRING,
    symbol: DataTypes.STRING,
    shares: DataTypes.INTEGER,
    price: DataTypes.FLOAT,
    timestamp: DataTypes.DATE,
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {});
  Trade.associate = function (models) {};
  return Trade;
};