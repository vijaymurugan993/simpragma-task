'use strict';

module.exports = {

    errorHandling: (err, res) => {
        res.status(500).json(err.message) // return error with status code of 500
        return;
    }

}