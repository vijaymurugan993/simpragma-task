'use strict';

const Models = require('../models'),
    utils = require('../utils/utils'),
    messages = require('../messages/result_messages.json'),
    error = require('../error/error');

module.exports = { // export functions to another js file

    createUser: async (req, res, next) => {
        try {
            req.body.password = await utils.hashPassword(req.body.password); // hashing given password
            Models.User.create(req.body) // sequelize create method 
                .then((result) => {
                    res.status(200).json(result) // return result with status code of 200
                }).catch((err) => {
                    error.errorHandling(err, res);
                });
        } catch (error) {
            next(error)
        }
    },

    updateUser: (req, res, next) => {
        try {
            Models.User.update({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: req.body.password
            }, {
                where: {
                    id: req.body.id
                }
            }).then(() => {
                res.status(200).json(messages.user.update)
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error)
        }

    },


    getUser: (req, res, next) => {
        try {
            Models.User.findOne({
                where: {
                    id: req.params.id,
                    isActive: true
                }
            }).then((result) => {
                if (result) {
                    res.status(200).json(result)
                } else {
                    res.status(400).json(messages.user.notFound)
                }
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error)
        }

    },

    gerUsers: (req, res, next) => {
        try {
            Models.User.findAll({
                where: {
                    isActive: true
                }
            }).then((result) => {
                if (result.length) {
                    res.status(200).json(result)
                } else {
                    res.status(400).json(messages.user.usersNotFound)
                }
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error)
        }

    },

    getUsersTrade: (req, res, next) => {
        try {
            Models.User.findOne({
                where: {
                    isActive: true,
                    id: req.params.id
                },
                include: [{
                    model: Models.Trade,
                    required: true
                }]
            }).then((result) => {
                if (result) {
                    res.status(200).json(result)
                } else {
                    res.status(400).json(messages.user.trade)
                }
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error);
        }
    },

    deleteUser: (req, res, next) => {
        try {
            Models.User.update({
                isActive: false
            }, {
                where: {
                    id: req.params.id
                }
            }).then(() => {
                res.status(200).json(messages.user.delete)
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error)
        }
    }
}