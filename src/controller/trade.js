'use strict';

const Models = require('../models'),
    utils = require('../utils/utils'),
    messages = require('../messages/result_messages.json'),
    error = require('../error/error');

const Sequelize = require('sequelize'),
    _ = require('lodash'),
    Op = Sequelize.Op;

module.exports = {

    createTrade: (req, res, next) => {
        try {
            Models.Trade.create(req.body)
                .then((result) => {
                    res.status(200).json(result) // return result with status code of 200
                }).catch((err) => {
                    error.errorHandling(err, res);
                });
        } catch (error) {
            next(error)
        }
    },

    updateTrade: (req, res, next) => {
        try {
            Models.Trade.update({
                type: req.body.type,
                symbol: req.body.symbol,
                shares: req.body.shares,
                price: req.body.price,
                timestamp: req.body.timestamp
            }, {
                where: {
                    id: req.body.id
                }
            }).then((result) => {
                res.status(200).json(messages.trade.update) // return result with status code of 200
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error)
        }
    },

    deleteTrade: (req, res, next) => {
        try {
            Models.Trade.update({
                isActive: false
            }, {
                where: {
                    id: req.params.id
                }
            }).then((result) => {
                res.status(200).json(messages.trade.delete) // return result with status code of 200
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error)
        }
    },

    getAllTrades: (req, res, next) => {
        try {
            Models.Trade.findAll({
                where: {
                    isActive: true
                }
            }).then((result) => {
                if (result.length) {
                    res.status(200).json(result) // return result with status code of 200
                } else {
                    res.status(400).json(messages.trade.notFound)
                }
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error)
        }
    },

    getUserTrade: (req, res, next) => {
        try {
            Models.Trade.findAll({
                where: {
                    isActive: true,
                    UserId: req.params.userId
                }
            }).then((result) => {
                if (result.length) {
                    res.status(200).json(result) // return result with status code of 200
                } else {
                    res.status(400).json(messages.trade.notFound)
                }
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error)
        }
    },

    getTradeType: (req, res, next) => {
        try {
            Models.Trade.findAll({
                where: {
                    isActive: true,
                    symbol: req.params.symbol,
                    type: req.params.type,
                    timestamp: {
                        [Op.between]: [req.params.startDate, req.params.endDate]
                    }
                }
            }).then((result) => {
                if (result.length) {
                    res.status(200).json(result) // return result with status code of 200
                } else {
                    res.status(400).json(messages.trade.notFound)
                }
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error);
        }
    },

    getTradeSymbol: (req, res, next) => {
        try {
            Models.Trade.findAll({
                where: {
                    isActive: true,
                    symbol: req.params.symbol,
                    timestamp: {
                        [Op.between]: [req.params.startDate, req.params.endDate]
                    }
                }
            }).then((result) => {
                if (result.length) {
                    const json = {
                        symbol: result,
                        lowest: _.minBy(result, min => {
                            return min.shares;
                        }),
                        highest: _.maxBy(result, max => {
                            return max.shares;
                        })
                    }
                    res.status(200).json(json) // return result with status code of 200
                } else {
                    res.status(400).json(messages.trade.notFound)
                }
            }).catch((err) => {
                error.errorHandling(err, res);
            });
        } catch (error) {
            next(error)
        }
    }

}