'use strict'

const express = require('express'),
    router = express.Router(),
    user = require('../controller/user');

router.post('/user', user.createUser);

router.put('/user', user.updateUser);

router.get('/user/:id', user.getUser);

router.get('/users', user.gerUsers);

router.get('/user-trade/:id', user.getUsersTrade);

router.delete('/user/:id', user.deleteUser);

module.exports = router