'use strict'

const express = require('express'),
    router = express.Router(),
    trade = require('../controller/trade');

router.post('/trade', trade.createTrade);

router.put('/trade', trade.updateTrade);

router.get('/trades', trade.getAllTrades);

router.get('/user-trades/:userId', trade.getUserTrade);

router.get('/trade-type/:symbol/:type/:startDate/:endDate', trade.getTradeType);

router.get('/trade-symbol/:symbol/:startDate/:endDate', trade.getTradeSymbol);

router.delete('/trade/:id', trade.deleteTrade);

module.exports = router