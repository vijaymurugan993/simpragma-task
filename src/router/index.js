'use strict'

module.exports = {
    user: require('./user'),
    trade: require('./trade')
}